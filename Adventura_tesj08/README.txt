Hra Haunted Hause je jednoduchá hra v textovém rozhraní, kde herní postava provádí úkony
na základě příkazů, které hráč píše do konzole. Cílem je dostat se z tajemného domu. K tomu je potřeba, aby hráč našel klíč
od hlavních dveří.
Author: Jakub Tesař, tesj08
Verze: 1.0
Komentář: Nelze mi spustit vytvořený .jar archiv - i po přeinstalování javy na počítači a odinstalování
přebytečných verzí. Hlášena chyba "A JNI error has occured, please check your installation and try again."
.jar archiv jsem vytvářel s hlavní třídou main.Start. Vím, že problém je v rozdílu verzí javy, v jakém jsem 
kód kompiloval a v jakém se jej snažím spustit, ale nevím si rady, jak to vyřešit. Ještě mám problém s kontrolou
pomocí PMD třídy Area, kdy mi vyhodí error "Error while parsing model.Area."