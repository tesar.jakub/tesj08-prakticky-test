package model;

/**     Třída implementující příkaz, který vyhodí předmět z hráčského inventáře a vloží jej do aktuální lokace.
 *      @Author: Jakub Tesař
 *      @Version 1.0
 *      @Created: 26.05.2020
 */
public class CommandThrowAway implements ICommand
{
    private static final String NAME = "vyhoď";
    private Inventory inventory;
    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandThrowAway(GamePlan plan)
    {
        this.plan = plan;
    }

    
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nechápu, co mám vyhodit. Musíš mi zadat nějakou věc";  // Pokud hráč nezadá žádný parametr (cíl cesty)
        } else if (parameters.length > 1) {
            return "Věci musím vyhazovat postupně, jinak se mi zamotají ruce";  // Pokud hráč zadá více parametrů
        }

        // Název cílové lokace si uložíme do pomocné proměnné
        
        // Zkusíme získat odkaz na cílovou lokaci
        Area area = plan.getCurrentArea();
        inventory = plan.getInventory();
        
        String itemName = parameters[0];
        Item item = inventory.getItem(itemName);
        if(!inventory.containsItem(itemName))
        {
            return "Tento předmět v inventáři nemáš!";
        }
        else
        {
            System.out.println(item.getName());
            inventory.removeFromInventory(item);
            area.addItem(item);
            return "Vyhodil jsi " + itemName + " z inventáře. Nyní leží na podlaze";
        }

        // Změníme aktuální lokaci (přesuneme hráče) a vrátíme popis nové lokace
        
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }

}
