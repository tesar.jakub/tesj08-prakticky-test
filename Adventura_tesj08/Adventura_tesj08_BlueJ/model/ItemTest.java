/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package model;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída {@code ItemTest} slouží ke komplexnímu otestování
 * třídy {@link ItemTest}.
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class ItemTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void testGetName()
    {
        Item item1 = new Item("predmet", "toto je predmet", true, true, null, null, false);
        Item item2 = new Item(null, "toto je predmet", true, true, null, null, false);
        assertEquals("predmet", item1.getName());
        assertFalse(item1.getName() != "predmet");
        assertNull(item2.getName());
    }


    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @Test
    public void testIsMoveable()
    {
        Item item1 = new Item("predmet", "toto je predmet", true, true, null, null, false);
        Item item2 = new Item("predmet2", "toto je druhy predmet", false, true, null, null, false);
        
        assertTrue(item1.isMoveable());
        assertFalse(item2.isMoveable());
        assertEquals(true, item1.isMoveable());
        assertEquals(false, item2.isMoveable());
    }
    @Test
    public void testIsPickable()
    {
        Item item1 = new Item("predmet", "toto je predmet", true, true, null, null, false);
        Item item2 = new Item("predmet2", "toto je druhy predmet", false, false, null, null, false);
        assertTrue(item1.isPickable());
        assertFalse(item2.isPickable());
        assertEquals(true, item1.isPickable());
        assertEquals(false, item2.isPickable());
    }
    @Test
    public void testGetSetDesciption()                      //funkcionalita datového atributu description
    {
        Item item1 = new Item("predmet", "toto je predmet", true, true, null, null, false);
        Item item2 = new Item("predmet2", "toto je druhy predmet", false, false, null, null, false);
        String firstDescription = item1.getDescription();
        String firstDescription2 = item2.getDescription();
        assertEquals("toto je predmet", item1.getDescription());
        assertEquals("toto je druhy predmet", item2.getDescription());
        
        item1.setDescription("toto je zmeneny predmet");
        item2.setDescription("toto je zmeneny druhy predmet");
        assertEquals("toto je zmeneny predmet", item1.getDescription());
        assertEquals("toto je zmeneny druhy predmet", item2.getDescription());
        assertFalse(item1.getDescription() == firstDescription);
        assertFalse(item2.getDescription() == firstDescription2);
    }
    
    @Test
    public void testContainer()                         //test containeru - add, remove, get
    {
        Item item1 = new Item("predmet", "toto je predmet", true, true, null, null, true);
        Item item2 = new Item("predmet2", "toto je vnitrni predmet", false, false, null, null, false);
        Item item3 = new Item("predmet3", "toto je druhy vnitrni predmet", false, false, null, null, false);
        
        item1.addToContainer(item2);
        
        assertEquals(item2, item1.getFromContainer(item2.getName()));
        
        assertNull(item1.getFromContainer(item3.getName()));
        
        item1.removeFromContainer(item2.getName());
        
        assertNull(item1.getFromContainer(item2.getName()));
        
        item1.addToContainer(item2);
        
        ArrayList<Item> testContainer = item1.getContainer();;
        assertTrue(testContainer.contains(item2));
        assertFalse(testContainer.contains(item3));
        
    }
    
}
