
package model;
import java.util.ArrayList;
import java.util.List;


/**     Třída slouží k vytvoření inventáře postavy (batohu) o určité velikosti. Obsahuje metody pro přidání, odebrání předmětu, zjištění přítomnosti
 *      předmětu, vrácení celého inventáře nebo textového řetězce s názvy předmětů v inventáři.
 *      @Author: Jakub Tesař
 *      @Version 1.0
 *      @Created: 24.05.2020
 */
public class Inventory
{
    protected ArrayList<Item> inventar =  new ArrayList<>();
    protected int size = 3;
    
    
    //metoda přidá do inventáře předmět obsažený v parametru
    public boolean addToInventory(Item vec)
    {
        if(vec.isPickable())
        {
            inventar.add(vec);
            
            return true;
        }
        else
        {
            System.out.println(vec.getName() + " se do inventáře vložit nedá!");
            return false;
           
        }
    }
    //metoda odebere z inventáře předmět obsažený v parametru
    public boolean removeFromInventory(Item vec)
    {
        if(inventar.contains(vec))
        {
            inventar.remove(vec);
            return true;
        }
        else
        {
            System.out.println("Tato věc se v inventáři nenachází.");
            return false;
        }   
    }
    //metoda zjistí, jestli inventář obsahuje předmět, jehož název je parametr
    public boolean containsItem(String itemName)
    {
        
        for(Item item : inventar)
        {
            if(item.getName().hashCode() == itemName.hashCode())
            {
                return true;
            }
        }
        return false;
    }
    //metoda vrátí předmět na základě jeho názvu, pokud se nachází v inventáři
    public Item getItem(String itemName)
    {
        for(Item item : inventar)
        {
            if(item.getName().hashCode() == itemName.hashCode())
            {
                return item;
            }
        }
        return null;
    }
    //vrátí List<> předmětů v inventáři
    public List<Item> getInventory()
    {
        return inventar;
    }
    //vrátí nastavenou maximální velikost inventáře
    public int getSize()
    {
        return size;
    }
    //vrátí počet předmětů v inventáři
    public int actualSize()
    {
        int size = 0;
        if(inventar != null)
        {
            for(Item vec : inventar)
        {
            size++;
        }
        }
        
        return size;
    }
    //vrátí textový řetězec názvů předmětů v inventáři
    public String getItemsString()
    {
        String items = "";
        for(Item item : inventar)
        {
            items = items + item.getName() + ", ";
        }
        return items;
    }
    

}
