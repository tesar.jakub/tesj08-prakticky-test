package model;
import java.util.*;


/**     Třída implementující příkaz, který zvedne předmět z herního plánu a vloží jej do inventáře.
 *      @Author: Jakub Tesař
 *      @Version 1.0
 *      @Created: 23.05.2020
 */
public class CommandPick implements ICommand
{
    private static final String NAME = "vezmi";
    private Inventory inventory;
    private GamePlan plan;
    
    public CommandPick(GamePlan plan)
    {
        this.plan = plan;
    }
    
    @Override
    public String process(String... parameters)
    {
            inventory = plan.getInventory();
          
            if (parameters.length == 0) {
                return "Nevím, co mám sebrat, musíš zadat název předmětu.";
            }
            
             if(parameters.length > 1 && parameters[0].hashCode() == 122)               // větev vezme item container ("vezmi Z CONTAINER ITEM")
            {
                Area area = plan.getCurrentArea();
                String boxName = parameters[1];
                String itemName = parameters[2];
                if(!area.containsItem(boxName))
                {
                    return "Nic takového k otevření tady není";
                }
                if(area.getItem(boxName).getFromContainer(itemName) == null)
                {
                    return "V " + boxName + " se " + itemName + " nenachází.";
                }
                
                
                Item item = area.getItem(boxName).getFromContainer(itemName);
                inventory = plan.getInventory();
                if(inventory.actualSize() < inventory.getSize())
                {
                    if(item.isPickable())
                        {
                            inventory.addToInventory(item);
                            area.getItem(boxName).removeFromContainer(itemName);
                            return "Z " + boxName +" jsi sebral předmět '" + itemName + "' a uložil jsi ho do inventáře.";
                        }
                        else
                        {
                            return "Předmět '" + itemName + "' se sebrat nedá.";
                        }
                    
                }
                else
                {
                    return "Inventář je plný!";
                }
            }
            
            
            
            
            if(parameters.length > 1 && parameters[0].hashCode() != 122)
            {
                return "Tomu nerozumím, neumím vzít více předmětů současně.";
            }
            
            String itemName = parameters[0];
            Area area = plan.getCurrentArea();
            
            
            if (!area.containsItem(itemName)) {
                return "Předmět '" + itemName + "' tady není.";
            }
            
            Item item = area.getItem(itemName);
            
            
           
            if(inventory.actualSize() < inventory.getSize() && parameters.length == 1)
            {
                if(item.isPickable())
                {
                    inventory.addToInventory(item);
                    area.removeItem(itemName);
                    return "Sebral(a) jsi předmět '" + itemName + "' a uložil jsi ho do inventáře.";
                }
                else
                {
                    return "Předmět '" + itemName + "' se sebrat nedá.";
                }
                
            }
            else
            {
                return "Inventář je plný!";
            }
            
            
        
        
    }

    @Override
    public String getName()
    {
        return NAME;
    }
    
    
}
