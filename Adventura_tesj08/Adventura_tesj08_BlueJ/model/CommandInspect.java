package model;

/**     Třída implementující příkaz, který "prozkoumá" herní předmět vrácením datového atributu item.description.
 *      @Author: Jakub Tesař
 *      @Version 1.0
 *      @Created: 23.05.2020
 */
public class CommandInspect implements ICommand
{
    private static final String NAME = "prozkoumej";
    private Inventory inventory;
    private GamePlan plan;
    
    public CommandInspect(GamePlan plan)
    {
        this.plan = plan;
    }
    @Override
    /** V případě, že je string parametru má více než 1 slovo a druhé je "z", metoda otevře box/krabici ve třetím slovu a vrátí seznam předmětů v ní
     *  obsažených. Slova s indexem >3 metoda nebere v potaz.
     * 
     * 
     * 
     */
    public String process(String... parameters)
    {
        inventory = plan.getInventory();
        if (parameters.length == 0) {
            return "Nevím, co mám prozkoumat, musíš zadat název předmětu.";
        }
        if(parameters.length > 1 && parameters[0].hashCode() == 118)               // větev prozkoumá container ("prozkoumej V CONTAINER ITEM")
        {
            Area area = plan.getCurrentArea();
            String boxName = parameters[1];
            String itemName = parameters[2];
            if(!area.containsItem(boxName))
            {
                return "Nic takového k otevření tady není.";
            }
            if(area.getItem(boxName).getFromContainer(itemName) == null)
            {
                return "V " + boxName + " se " + itemName + " nenachází.";
            }
            return area.getItem(boxName).getFromContainer(itemName).getDescription();
        }
        if(parameters.length > 1 && parameters[0].hashCode() != 118)
        {
            return "Tomu nerozumím, neumím prozkoumat více předmětů současně.";
        }
        
        
        String itemName = parameters[0];
        Area area = plan.getCurrentArea();
        if(itemName.hashCode() == 627555595)
        {
            inventory = plan.getInventory();
            if(inventory.getItemsString() == "")
            {
                return "V inventáři nic není!";
            }
            else
            {
                return inventory.getItemsString();
            }
        
        }
        // Prozkoumávaný předmět může být v aktuální lokaci, ale i v inventáři
        
        // (pseudo-kód, jak by to asi mohlo vypadat, konkrétní
        // implementace ve Vaší hře se může lišit)
        
        /* Inventory inventory = plan.getInventory();
         *
         * if (!area.containsItem(itemName) && !inventory.containsItem(itemName)) {
         *     ... */
        
        
        if (!area.containsItem(itemName) && !inventory.containsItem(itemName)) {
            return "Předmět '" + itemName + "' tady není.";
        }
        else
        {
            if(inventory.getItem(itemName) != null)
            {
                return inventory.getItem(itemName).getDescription();
            }
            else
            {
                return area.getItem(itemName).getDescription();
            }
        }
        // Výpis popisu předmětu z inventáře
        
        // (pseudo-kód, jak by to asi mohlo vypadat, konkrétní
        // implementace ve Vaší hře se může lišit)
        
        /* if (inventory.containsItem(itemName)) {
         *     return inventory.getItem(itemName).getDescription();
         * } */

        
    }
    @Override
    public String getName()
    {
        return NAME;
    }
}
