package model;

/**     Třída implementující příkaz, který odemkne zamknutou místnost za předpokladu, že hráč má v inventáři odpovídající klíč.
 *      @Author: Jakub Tesař
 *      @Version 1.0
 *      @Created: 27.05.2020
 */
public class CommandUnlock implements ICommand
{
    private static final String NAME = "odemkni";
    private Inventory inventory;
    private GamePlan plan;

    /**
     * Konstruktor třídy.
     *
     * @param plan odkaz na herní plán s aktuálním stavem hry
     */
    public CommandUnlock(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda zjistí, jestli se lokace v parametru nachází vedle aktuální lokace, zjistí, jestli od lokace máme příslušný klíč, pokud ano, tak
     * nastaví datový atribut Unlocked cílové lokace na true a předmět klíče odstraní z inventáře.
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nechápu, co mám odemknout. Musíš mi zadat nějaký cíl.";  // Pokud hráč nezadá žádný parametr (cíl cesty)
        } else if (parameters.length > 1) {
            return "Nechápu, co po mě chceš. Neumím se 'rozdvojit' a odemknout více dveří současně.";  // Pokud hráč zadá více parametrů
        }

        // Název cílové lokace si uložíme do pomocné proměnné
        String exitName = parameters[0];

        // Zkusíme získat odkaz na cílovou lokaci
        Area exitArea = plan.getCurrentArea().getExitArea(exitName);
        
        inventory = plan.getInventory();
        if (exitArea == null) {
            return "Dveře od této místnosti zde nejsou!";  // Pokud hráč zadal název lokace, která nesousedí s aktuální
        }
        if(exitArea.isUnlocked())
        {
            return "Tyto dveře již odemknuté jsou!";
        }
        if(!inventory.containsItem(exitArea.getKey()))
        {
            return "Klíč od těchto dveří nemáš!";
        }
        
        exitArea.setUnlocked(true);
        inventory.removeFromInventory(inventory.getItem(exitArea.getKey()));
        return "Místnost byla odemknuta";
        

    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo {@value NAME}.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }

}
