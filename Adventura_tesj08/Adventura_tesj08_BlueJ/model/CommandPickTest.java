/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package model;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída {@code CommandPickTest} slouží ke komplexnímu otestování
 * třídy {@link CommandPickTest}.
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class CommandPickTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void testCommandPick()
    {
        assertEquals("ložnice", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi schodiště");
        assertEquals("schodiště", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi obývací_pokoj");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi kuchyně");
        
        assertFalse(game.isGameOver());
        
        assertEquals(game.processCommand("vezmi nuz"), "Sebral(a) jsi předmět 'nuz' a uložil jsi ho do inventáře.");
        assertEquals(game.processCommand("vezmi z lednice automobil"), "V lednice se automobil nenachází.");
        assertEquals(game.processCommand("vezmi z lednice kolac"), "Z lednice jsi sebral předmět 'kolac' a uložil jsi ho do inventáře.");
        assertEquals(game.processCommand("vezmi hrnec"), "Předmět 'hrnec' se sebrat nedá.");
        game.processCommand("zvedni hrnec");
        assertEquals(game.processCommand("vezmi poukaz_do_háemka"), "Sebral(a) jsi předmět 'poukaz_do_háemka' a uložil jsi ho do inventáře.");
        
        game.processCommand("jdi obývací_pokoj");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("zvedni koberec");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("vezmi klic_od_komory");
        assertEquals(game.processCommand("vezmi klic_od_komory"), "Inventář je plný!");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        
        
        
        
        
    }
            
    @After
    public void tearDown()
    {
    }



//\TT== TESTS PROPER ===========================================================

    /***************************************************************************
     * Test of the {@link #setUp()} method preparing the test fixture.
     */
    @Test
    public void testSetUp()
    {
    }

}
