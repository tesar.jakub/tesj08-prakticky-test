/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package model;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída {@code CommandLiftTest} slouží ke komplexnímu otestování
 * třídy {@link CommandLiftTest}.
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class CommandLiftTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void testCommandPick()
    {
        assertEquals("ložnice", game.getGamePlan().getCurrentArea().getName());
        assertEquals(game.processCommand("zvedni lampa"), "Zvednul jsi předmět lampa");
        assertEquals(game.processCommand("zvedni postel"), "Toto opravdu nezvedneš.");
        game.processCommand("jdi schodiště");
        assertEquals("schodiště", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi obývací_pokoj");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        
        assertEquals(game.processCommand("zvedni koberec"), "Klasický vzorovaný arabský koberec./n Objevil jsi klic_od_komory");
        game.processCommand("jdi kuchyně");
        
        assertFalse(game.isGameOver());
        
        assertEquals(game.processCommand("zvedni náklaďák"), "Předmět náklaďák tady není.");
        assertEquals(game.processCommand("zvedni inventar"), "Pro nahlédnutí do inventáře použij příkaz 'prozkoumej'");
        assertEquals(game.processCommand("zvedni"), "Nevím, co mám zvednout, musíš zadat název předmětu.");
        
        
        
        
        
        
        
        
        
    }
    @After
    public void tearDown()
    {
    }



}
