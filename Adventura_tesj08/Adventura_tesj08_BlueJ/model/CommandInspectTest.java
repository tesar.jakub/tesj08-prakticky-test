/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package model;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída {@code CommandInspectTest} slouží ke komplexnímu otestování
 * třídy {@link CommandInspectTest}.
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class CommandInspectTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }
    @Test
    public void testInspectItem()
    {
        assertEquals("ložnice", game.getGamePlan().getCurrentArea().getName());
        assertEquals(game.processCommand("prozkoumej lampa"), "Lampa, která nesvítí.");
        assertEquals(game.processCommand("prozkoumej inventar"), "V inventáři nic není!");
        
        
        game.processCommand("jdi schodiště");
        assertEquals("schodiště", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi obývací_pokoj");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        
        assertEquals(game.processCommand("zvedni koberec"), "Klasický vzorovaný arabský koberec./n Objevil jsi klic_od_komory");
        assertEquals(game.processCommand("vezmi klic_od_komory"), "Sebral(a) jsi předmět 'klic_od_komory' a uložil jsi ho do inventáře.");
        assertEquals(game.processCommand("prozkoumej inventar"), "klic_od_komory, ");
        assertEquals(game.processCommand("prozkoumej"), "Nevím, co mám prozkoumat, musíš zadat název předmětu.");
        
        
        game.processCommand("jdi kuchyně");
        
        assertFalse(game.isGameOver());
        
        
        assertEquals(game.processCommand("prozkoumej v lednice kolac"), "Cheesecake s jahodami na povrchu");
        assertEquals(game.processCommand("prozkoumej v lednice koloběžka"), "V lednice se koloběžka nenachází.");
        assertEquals(game.processCommand("prozkoumej v mraznička kolac"), "Nic takového k otevření tady není.");
        assertEquals(game.processCommand("prozkoumej pat a mat"), "Tomu nerozumím, neumím prozkoumat více předmětů současně.");
        
    }

    
    @After
    public void tearDown()
    {
    }



}
