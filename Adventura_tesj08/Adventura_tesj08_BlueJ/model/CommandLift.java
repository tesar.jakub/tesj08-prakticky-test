package model;
import java.util.*;

/**     Třída implementující příkaz, který zvedne předmět z herního plánu a zjistí, zda-li se pod předmětem něco nenachází.
 *      @Author: Jakub Tesař
 *      @Version 1.0
 *      @Created: 25.05.2020
 */
public class CommandLift implements ICommand
{
    private static final String NAME = "zvedni";
   
    private GamePlan plan;
    
    public CommandLift(GamePlan plan)
    {
        this.plan = plan;
    }
    @Override
    /** Metoda vyhledá předmět jak v aktuální lokaci, tak inventáři, a následně vrátí datový atribut description předmětu.
     */
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nevím, co mám zvednout, musíš zadat název předmětu.";
        }
        
        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím zvednout více předmětů současně.";
        }
        
        String itemName = parameters[0];
        Area area = plan.getCurrentArea();
                
        
        
        if(itemName.hashCode() == "inventar".hashCode())
        {
            return "Pro nahlédnutí do inventáře použij příkaz 'prozkoumej'";
        }
        if (!area.containsItem(itemName)) 
        {
            System.out.println(itemName);
            return "Předmět " + itemName + " tady není.";
        }
        
        if(!area.getItem(itemName).isMoveable())
        {
            return "Toto opravdu nezvedneš.";
        }
        else
        {
            if(found(area, itemName) != null)
            {
                 
                 
                 return area.getItem(itemName).getDescription() + "/n Objevil jsi " + found(area, itemName).getItemUnder().getName();
            }
            else
            {
                return "Zvednul jsi předmět " + area.getItem(itemName).getName();
            }
             
           
        }
        // Výpis popisu předmětu z inventáře
        
        // (pseudo-kód, jak by to asi mohlo vypadat, konkrétní
        // implementace ve Vaší hře se může lišit)
        
        /* if (inventory.containsItem(itemName)) {
         *     return inventory.getItem(itemName).getDescription();
         * } */

        
    }
    @Override
    public String getName()
    {
        return NAME;
    }
    
    /*metoda vrací předmět, který je nalezen po zvednutí jiného předmětu
    * datový atribut itemUnder třídy Item
    */
    
    public Item found(Area area, String itemName)                       
    {
        Item foundItem = area.getItem(itemName).getItemUnder();
        if(foundItem != null)
        {
            Map<String,Item> items = area.getItems();
            area.addItem(foundItem);
            return items.get(itemName);
        }
        else
        {
            return null;
        }
    }

}
