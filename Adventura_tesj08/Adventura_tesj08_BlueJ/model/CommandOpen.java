package model;
import java.util.*;

/**     Třída implementující příkaz, který otevře předmět s vnitřním prostorem (krabice, box, lednička) a vrátí String s předměty, které se v krabici, boxu
 *      či lednici nachází.
 *      @Author: Jakub Tesař
 *      @Version 1.0
 *      @Created: 26.05.2020
 */
public class CommandOpen implements ICommand
{
    private static final String NAME = "otevři";
    
    private GamePlan plan;
    
    public CommandOpen(GamePlan plan)
    {
        this.plan = plan;
    }
    @Override
    public String process(String... parameters)
    {
        
        if (parameters.length == 0) {
            return "Nevím, co mám otevřít, musíš zadat název předmětu.";
        }
        
        if(parameters.length >1)
        {
            return "Tomu nerozumím, neumím otevřít více předmětů současně.";
        }
        
        
        String itemName = parameters[0];
        Area area = plan.getCurrentArea();
        
        
        if(itemName.hashCode() == "inventar".hashCode())
        {
            return "Pro nahlédnutí do inventáře použij příkaz 'prozkoumej'";
        }
        if (!area.containsItem(itemName) ) {
            return "Předmět '" + itemName + "' tady není.";
        }
        if(!area.getItem(itemName).getBox())
        {
            return itemName + " se fakt otevřít nedá.";
        }
        
        
        else
        {
            ArrayList<Item> obsah = area.getItem(itemName).getContainer();
            if(obsah.size() == 0)
            {
                return "Je to prázdné.";
            }
            String itemsInBox = "";
            for(Item item : obsah)
            {
                itemsInBox = itemsInBox + item.getName() + ", ";
            }
            return "Je zde: " + itemsInBox;
        }
        
        
    }
    @Override
    public String getName()
    {
        return NAME;
    }
}
