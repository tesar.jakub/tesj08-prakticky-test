/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package model;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída {@code InventoryTest} slouží ke komplexnímu otestování
 * třídy {@link InventoryTest}.
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class InventoryTest
{
    
    
    @Before
    public void setUp()
    {
    }
    
    @Test
    public void testAddToInventory()
    {
        Item item1 = new Item("predmet", "prvni predmet", true, true, null, null,false);              //vyrábění lokálního inventáře
        Item item2 = new Item("predmet2", "druhy predmet", true, false, null, null, false);
        Inventory inventar = new Inventory();
        
        
        inventar.addToInventory(item1); 
        
        ArrayList<Item> testinventar = new ArrayList<>();                               //test metody 
        testinventar = inventar.inventar;  
        assertTrue(testinventar.contains(item1));
        assertFalse(testinventar.contains(item2));
        
        
    }
    @Test
    public void testContainsItem()
    {
        Item item1 = new Item("predmet", "prvni predmet", true, true, null, null, false);
        Item item2 = new Item("predmet2", "druhy predmet", true, false, null, null, false);
        Inventory inventar = new Inventory();
        ArrayList<Item> testinventar = new ArrayList<>();
        testinventar = inventar.inventar;
        
        
        inventar.addToInventory(item1);
        assertTrue(testinventar.contains(item1));
        assertFalse(testinventar.contains(item2));
        
        
    }
    @Test
    public void testRemoveFromInventory()
    {
        Item item1 = new Item("predmet", "prvni predmet", true, true, null, null, false);
        Item item2 = new Item("predmet2", "druhy predmet", true, false, null, null, false);
        Inventory inventar = new Inventory();
        ArrayList<Item> testinventar = new ArrayList<>();
        
        inventar.addToInventory(item1);
        assertTrue(inventar.containsItem(item1.getName()));
        inventar.removeFromInventory(item1);
        assertFalse(inventar.containsItem(item1.getName()));
    }
    @Test
    public void testGetItem()
    {
        Item item1 = new Item("predmet", "prvni predmet", true, true, null, null, false);
        Item item2 = new Item("predmet2", "druhy predmet", true, false, null, null, false);
        Inventory inventar = new Inventory();
        
        
        inventar.addToInventory(item1);
        assertEquals(item1, inventar.getItem(item1.getName()));
        assertNull(inventar.getItem(item2.getName()));
    }
    @Test
    public void testGetInventory()
    {
        Item item1 = new Item("predmet", "prvni predmet", true, true, null, null, false);
        Item item2 = new Item("predmet2", "druhy predmet", true, false, null, null, false);
        Inventory inventar = new Inventory();
        

        inventar.addToInventory(item1);
        inventar.addToInventory(item2);
        assertEquals(inventar.inventar, inventar.getInventory());
        
    }
    @Test
    public void testGetSize()
    {
        Inventory inventar = new Inventory();
        assertEquals(inventar.size, inventar.getSize());
    }
    @Test
    public void testActualSize()
    {
        Item item1 = new Item("predmet", "prvni predmet", true, true, null, null, false);
        Item item2 = new Item("predmet2", "druhy predmet", true, false, null, null, false);
        Inventory inventar = new Inventory();
        

        inventar.addToInventory(item1);
        inventar.addToInventory(item2);
        int count = 0;
        for(Item item : inventar.getInventory())
        {
            count++;
        }
        assertEquals(count, inventar.actualSize());
    }
    @Test
    public void testGetItemsString()
    {
        Item item1 = new Item("predmet", "prvni predmet", true, true, null, null, false);
        Item item2 = new Item("predmet2", "druhy predmet", true, false, null, null, false);
        Inventory inventar = new Inventory();
        String items = "";
        for(Item item : inventar.getInventory())
        {
            items = items + item.getName() + ", ";
        }
        assertEquals(items, inventar.getItemsString());
    }
    

    @After
    public void tearDown()
    {
    }
}
