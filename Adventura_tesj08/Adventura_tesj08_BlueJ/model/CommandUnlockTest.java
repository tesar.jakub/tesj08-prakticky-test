

/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package model;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/**
 * @author  Jakub Tesař
 * @version 03.06.2020
 */
public class CommandUnlockTest
{
private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }
    @Test
    public void testUnlock()
    {
        assertEquals("ložnice", game.getGamePlan().getCurrentArea().getName());
        assertEquals(game.processCommand("zvedni lampa"), "Zvednul jsi předmět lampa");
        assertEquals(game.processCommand("zvedni postel"), "Toto opravdu nezvedneš.");
        game.processCommand("jdi schodiště");
        assertEquals("schodiště", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        assertEquals(game.processCommand("jdi komora"), "Tyto dveře jsou zamknuté!");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        assertEquals(game.processCommand("odemkni komora"), "Klíč od těchto dveří nemáš!");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi obývací_pokoj");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        
        assertEquals(game.processCommand("zvedni koberec"), "Klasický vzorovaný arabský koberec./n Objevil jsi klic_od_komory");
        game.processCommand("vezmi klic_od_komory");
        
        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        assertEquals(game.processCommand("odemkni komora"), "Místnost byla odemknuta");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi komora");
        assertEquals("komora", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        
        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        assertEquals(game.processCommand("odemkni komora"), "Tyto dveře již odemknuté jsou!");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        
        assertEquals(game.processCommand("odemkni kuchyň"), "Dveře od této místnosti zde nejsou!");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        assertFalse(game.isGameOver());
        
    }

}
