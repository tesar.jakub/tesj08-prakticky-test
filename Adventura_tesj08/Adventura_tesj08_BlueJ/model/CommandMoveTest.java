/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package model;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída {@code CommandMoveTest} slouží ke komplexnímu otestování
 * třídy {@link CommandMoveTest}.
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class CommandMoveTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }
    @Test
    public void testMove()
    {
        assertEquals("ložnice", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi schodiště");
        assertEquals("schodiště", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi obývací_pokoj");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
     
       
        assertEquals(game.processCommand("zvedni koberec"), "Klasický vzorovaný arabský koberec./n Objevil jsi klic_od_komory");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        assertEquals(game.processCommand("vezmi klic_od_komory"), "Sebral(a) jsi předmět 'klic_od_komory' a uložil jsi ho do inventáře.");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        
        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("odemkni komora");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi komora");
        assertEquals("komora", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        
        
    }

}
