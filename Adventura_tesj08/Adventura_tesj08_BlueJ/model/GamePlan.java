package model;

import java.util.*;


/**     
 * Třída představující aktuální stav hry. Veškeré informace o stavu hry
 * <i>(mapa lokací, inventář, vlastnosti hlavní postavy, informace o plnění
 * úkolů apod.)</i> by měly být uložené zde v podobě datových atributů.
 * <p>
 * Třída existuje především pro usnadnění potenciální implementace ukládání
 * a načítání hry. Pro uložení rozehrané hry do souboru by mělo stačit uložit
 * údaje z objektu této třídy <i>(např. pomocí serializace objektu)</i>. Pro
 * načtení uložené hry ze souboru by mělo stačit vytvořit objekt této třídy
 * a vhodným způsobem ho předat instanci třídy {@link Game}.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan �?íha
 * @version LS 2020
 *
 * @see <a href="https://java.vse.cz/4it101/AdvSoubory">Postup pro implementaci ukládání a načítání hry na předmětové wiki</a>
 * @see java.io.Serializable
 */
public class GamePlan
{
    private static final String FINAL_LOCATION_NAME = "ven";
    Inventory inventar = new Inventory();
    public Area currentArea;
    
    
        
  

    /**
     * Konstruktor třídy. Pomocí metody {@link #prepareWorldMap() prepareWorldMap}
     * vytvoří jednotlivé lokace a propojí je pomocí východů.
     */
    public GamePlan()
    {
        prepareWorldMap();
    }

    /**
     * Metoda vytváří jednotlivé lokace a propojuje je pomocí východů. Jako
     * výchozí aktuální lokaci následně nastaví domeček, ve kterém bydlí
     * Karkulka.
     */
    private void prepareWorldMap()
    {
        Area loznice = new Area("ložnice","Je zde postel, skříň a lampa.", true, null);
        Area schodiste = new Area("schodiště", "Tudy se jde nahoru/dolů. Nic zde není", true, null);
        Area predsin = new Area("předsín","Zde je východ ven. ", true, null);
        Area komora = new Area("komora","Je zde šero a slyšíš strašidelné zvuky. ", false, "klic_od_komory");
        Area obyvaci_pokoj = new Area("obývací_pokoj", "Elegantní obývací pokoj s krbem", true, null);
        Area kuchyn = new Area("kuchyně", "Starobylá kuchyně se šachovnicovitou podlahou.", true, null);
        Area spojovaci_chodba = new Area("spojovací_chodba", "Prostorná hala tak velká, že lze vytvořit ozvěnu.", true, null);
        Area venek = new Area("ven","", false, "klic_od_venku");
        // Hru začneme v ložnici
        currentArea = loznice;
        // Nastavíme průchody mezi lokacemi (sousední lokace)
        loznice.addExit(schodiste);
        
        
        
        schodiste.addExit(loznice);
        schodiste.addExit(spojovaci_chodba);
        
        spojovaci_chodba.addExit(schodiste);
        spojovaci_chodba.addExit(komora);
        spojovaci_chodba.addExit(predsin);
        spojovaci_chodba.addExit(obyvaci_pokoj);

        komora.addExit(spojovaci_chodba);
        
        predsin.addExit(venek);
        predsin.addExit(spojovaci_chodba);
        
        obyvaci_pokoj.addExit(spojovaci_chodba);
        obyvaci_pokoj.addExit(kuchyn);
        
        kuchyn.addExit(obyvaci_pokoj);
        
        
        // Přidáme předměty do lokací
        Item postel = new Item("postel", "Manželská postel s nebesy", false, false, null, null, false);
        Item lampa = new Item("lampa", "Lampa, která nesvítí.", true, false, null, null, false);
        Item kvetinac = new Item("kvetinac", "Květináč ve kterém je fíkus.",false, false, null, null, false);
        Item klic_od_komory = new Item("klic_od_komory", "Toto je klíč od komory", false, true, null, "komora", false);
        Item koberec = new Item("koberec", "Klasický vzorovaný arabský koberec.", true, false, klic_od_komory, null, false);
        Item nuz = new Item("nuz", "Toto je nůž, který tě ochrání před zlými duchy", false, true, null, null, false);
        Item donut = new Item("donut", "Donut s barevnou posypkou a čokoládovou náplní.", false, true, null, null, false);
        Item poukaz = new Item("poukaz_do_háemka", "Poukaz do řetězce H&M v hodnotě 500 korun", false, true, null, null, false);
        Item hrnec = new Item("hrnec", "Plechový hrnec se spálenými těstovinami", true, false, poukaz, null, false);
        
        
        loznice.addItem(postel);
        loznice.addItem(lampa);
        spojovaci_chodba.addItem(lampa);
        obyvaci_pokoj.addItem(koberec);
        kuchyn.addItem(nuz);
        loznice.addItem(donut);
        kuchyn.addItem(hrnec);
        spojovaci_chodba.addItem(kvetinac);
        
        //Přidám containery do lokací
        Item krabice = new Item("krabice", "Kartonová stěhovací krabice", true,false, null, null, true);
        Item lednice = new Item("lednice", "Velká americká dvoukřídlá lednice", false,false, null, null, true);
        Item kolac = new Item("kolac", "Cheesecake s jahodami na povrchu", false, true, null, null, false);
        Item hamburger = new Item("hamburger", "Hamburger z mekáče. Vypadá pořád stejně.", false, true, null, null, false);
        Item kapesnik = new Item("kapesnik", "Posmrkaný kapesník. Fuj", false, true, null, null, false);
        Item klic_od_venku = new Item("klic_od_venku", "Klíč od hlavních dveří.", false, true, null, "venek", false);
        lednice.addToContainer(kolac);
        lednice.addToContainer(hamburger);
        krabice.addToContainer(kapesnik);
        krabice.addToContainer(klic_od_venku);
        
        
        komora.addItem(krabice);
        kuchyn.addItem(lednice);
    }
    public void addToArea(Area area, Item item)
    {
        area.addItem(item);
    }
    
    /**
     * Metoda vrací odkaz na aktuální lokaci, ve které se hráč právě nachází.
     *
     * @return aktuální lokace
     */
    public Area getCurrentArea()
    {
        return currentArea;
    }

    /**
     * Metoda nastaví aktuální lokaci, používá ji příkaz {@link CommandMove}
     * při přechodu mezi lokacemi.
     *
     * @param area lokace, která bude nastavena jako aktuální
     */
    public void setCurrentArea(Area area)
    {
        currentArea = area;
    }
    
    public boolean isVictorious()
    {
        return FINAL_LOCATION_NAME.equals(currentArea.getName());
    }
    
    //vrátí inventář
    public Inventory getInventory()
    {
        return inventar;
    }
    //zjistí, jestli inventář obsahuje herní předmět
    public boolean doesInventoryContain(String itemname)
    {
        if(inventar.containsItem(itemname))
        {
            return true;
        }
        else
        {
            return false;
        }
    
    }
}
