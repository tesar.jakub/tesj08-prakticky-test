package model;



import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování herního příběhu.
 *
 * @author Jarmila Pavlíčková
 * @author Jan �?íha
 * @version LS 2020
 */
public class GameTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void testPlayerQuit()
    {
        assertEquals("ložnice", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi schodiště");
        assertEquals("schodiště", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("konec");
        assertTrue(game.isGameOver());
    }
    @Test
    public void testPlayerWon()
    {
        assertEquals("ložnice", game.getGamePlan().getCurrentArea().getName());

        game.processCommand("jdi schodiště");
        assertEquals("schodiště", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi obývací_pokoj");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("zvedni koberec");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("vezmi klic_od_komory");
        assertEquals("obývací_pokoj", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("odemkni komora");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi komora");
        assertEquals("komora", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("vezmi z krabice klic_od_venku");
        assertEquals("komora", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi spojovací_chodba");
        assertEquals("spojovací_chodba", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi předsín");
        assertEquals("předsín", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("odemkni ven");
        assertEquals("předsín", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());
        
        game.processCommand("jdi ven");
        assertEquals("ven", game.getGamePlan().getCurrentArea().getName());
        assertTrue(game.isGameOver());
    }

}
