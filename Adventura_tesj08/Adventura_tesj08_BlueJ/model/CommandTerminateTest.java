/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package model;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;



/*******************************************************************************
 * Testovací třída {@code CommandTerminateTest} slouží ke komplexnímu otestování
 * třídy {@link CommandTerminateTest}.
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class CommandTerminateTest
{

   
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }
    @Test
    public void testCommandTerminate()
    {
        assertEquals(game.processCommand("konec XXX"), "Nechápu, co mám ukončit. Příkaz 'konec' se používá bez parametrů a ukončuje celou hru.");
        assertFalse(game.isGameOver());
        assertEquals(game.processCommand("konec"), "Hra byla ukončena příkazem 'konec'.");
        assertTrue(game.isGameOver());
    }


    @After
    public void tearDown()
    {
    }

  
    

}
