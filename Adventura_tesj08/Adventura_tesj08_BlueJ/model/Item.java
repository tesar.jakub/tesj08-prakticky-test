package model;
import java.util.*;
/** Třída Item slouží k vytváření instancí herních předmětů a "krabicí" - předmětů s vnitřním prostorem, který je řešen ArrayListem. 
 *      Obsahuje gettery na datové atributy a settery na část datových atributů.
 *      @Author: Jakub Tesař
 *      @Version 1.0
 *      @Created: 22.05.2020
 */


public  class Item
{
    private String name;
    private String description;
    private boolean moveable;
    private boolean pickable;
    private Item itemUnder;
    private String opens;
    private ArrayList<Item> container;
    private boolean box;

    public Item(String name, String description, boolean moveable, boolean pickable, Item itemUnder, String opens, boolean box)
    {
        this.name = name;
        this.description = description;
        this.moveable = moveable;
        this.pickable = pickable;
        this.itemUnder = itemUnder;
        this.opens = opens;
        this.container = new ArrayList<>();
        this.box = box;
    }

    public Item(String name, String description)
    {
        this(name, description, true, true, null, null, false);
    }

    public String getName()
    {
        return name;
    }
    //vrátí ArrayList vnitřního prostoru
    public boolean getBox() 
    {
        return box;
    }
    //vrátí popis
    public String getDescription()
    {
        return description;
    }
    //nastaví popis
    public void setDescription(String description)
    {
        this.description = description;
    }
    //vrátí, jestli je předmět zvednutelný
    public boolean isMoveable()
    {
        return moveable;
    }
    //vrátí, jestli je předmět sebratelný
    public boolean isPickable()
    {
        return pickable;
    }
    //setter zvednutelnosti
    public void setMoveable(boolean moveable)
    {
        this.moveable = moveable;
    }
    //vrátí předmět, který se nachází pod předmětem
    public Item getItemUnder()
    {
        return itemUnder;
    }
    //používá se u klíče, vrací string s názvem místnosti, kterou klíč otevírá
    public String getOpens()
    {
        return opens;
    }
    
    
    //pracuje s containerem
    public ArrayList<Item> getContainer()
    {
        return container;
    }
    //přidá do vnitřního prostoru
    public void addToContainer(Item item)
    {
        container.add(item);
    }
    //vrací předmět z vnitřního prostoru
    public Item getFromContainer(String itemName)
    {
        for(Item item : container)
        {
            
            if(item.getName().hashCode() == itemName.hashCode())
            {
                return item;
            }
        }
        return null;
    }
    //odstraňuje z vnitřního prostoru
    public void removeFromContainer(String itemName)
    {
       
        for(Item item : container)
        {
            
            if(item.getName().hashCode() == itemName.hashCode())
            {
                container.remove(item);
                break;
            }
            
        }
        
    }

}
